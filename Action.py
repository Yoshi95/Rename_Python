import os
from glob import glob
from os.path import join

class Action:
   def __init__(self, nomdurepertoire="",regle=""):
      self.nomdurepertoire = nomdurepertoire
      self.regle = regle

   def _get_nomdurepertoire(self):
      return self.nomdurepertoire
   def _get_regle(self):
      return self.regle

   def simule(self):
       return "rien à marquer"

   def __str__(self):
       return "Nom du répertoire " + self.nomdurepertoire + "/n" + "Regle " + self.regle
