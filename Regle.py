class Regle:
   """
   Class Règle
   """
   def __init__(self,amorce="", apartirde="", prefixe="", nomfichier="", postfixe="", extension=""):
      """
      :param amorce: Trois choix possible : Aucun, Chiffre ou Lettre
      :param apartirde: Qui sera un texte
      :param prefixe: Qui sera un texte
      :param nomfichier: False si on reprend pas le nom du fichier et True si on le reprend
      :param postfixe:
      :param extension: Collection de dictionnaire
      """
      self.amorce = amorce
      self.apartirde = apartirde
      self.prefixe = prefixe
      self.nomfichier = nomfichier
      self.postfixe = postfixe
      self.extension = extension

   def _get_amorce(self):
      return self.amorce
   def _get_apartide(self):
      return self.apartirde
   def _get_prefixe(self):
      return self.prefixe
   def _get_nomfichier(self):
      return self.nomfichier
   def _get_postfixe(self):
      return self.postfixe
   def _get_extension(self):
      return self.extension

   def _set_amorce(self,amorce):
      self.amorce = amorce
   def _set_apartirde(self,apartirde):
      self.apartirde = apartirde
   def _set_prefixe(self,prefixe):
      self.prefixe = prefixe
   def _set_nomfichier(self,nomfichier):
      self.nomfichier = nomfichier
   def _set_postfixe(self,postfixe):
      self.postfixe = postfixe
   def _set_extension(self,extension):
      self.extension = extension

   def __str__(self):
      return "Amorce : " + self.amorce + "/n" + "A Partir de " + self.apartirde  + "/n" + "Prefixe " + self.prefixe + "/n" + "Nom Fichier " + self.nomfichier + "/n" + "PostFixe " + self.postfixe + "/n" + "Extension " + self.extension + "/n"