from Action import *
from tkinter import messagebox
import os
from Regle import *

class Renommage(Action):
   def __init__(self, nomdurepertoire, regle):
      Action.__init__(self, nomdurepertoire,regle)

   def renommer(self):
      simulation = Action.simule(self)
      message = simulation[0]
      dict = simulation[1]
      if message != "Erreur, une ou plusieurs extensions saisies ne sont pas dans le répertoire":
         question = messagebox.askyesno("Prévirtualisation", "Confirmer ? : \n\n" + message, icon="question")
         if question == True:
            for original in dict.items():
                os.rename(original)
            messagebox.showinfo("Ok", "Renommer OK !")
         return True
      else:
         messagebox.showerror("Erreur", message)
         return False

   def __str__(self):
      return