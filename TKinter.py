from tkinter import filedialog
from tkinter import *
from Action import *
from ListeRegle import *
from Regle import *
from Renomage import *
from second import *
from Lister import *

class Interface:
  def __init__(self):

   """Fenetre"""
   self.home = Tk()
   self.home.title("Renommage de Fichier - Python")
   self.home.geometry("1000x500")
   self.home.resizable(height=FALSE, width=FALSE)

   """Label vide pour placement des éléments"""
   self.Label_Vide = Label(self.home, text="")
   self.Label_Vide.grid(row=3, column=3)
   self.Label_Vide.grid(row=3, column=2)

   """Frame 1 = Label & Input pour le nom du répertoire"""
   self.Frame1 = Frame(self.home, borderwidth=2, relief=GROOVE)
   self.Frame1.grid(row=2,column=3)
   self.label_rename = Label(self.Frame1, text="Renommer en lots")
   self.label_rename.grid(row=1, column=1)
   self.label_name = Label(self.Frame1, text="Nom du répertoire")
   self.label_name.grid(row=2, column=1)
   self.Input_Chemin = Entry(self.Frame1, text="Chemin ?",width=30)
   self.Input_Chemin.grid(row=3, column=1)
   #self.home.filename = filedialog.askopenfilename(initialdir="/", title="Select file", filetypes=(("jpeg files", "*.jpg"), ("all files", "*.*")))

   """Frame pour la photo"""
   self.photo = PhotoImage(file="logo.gif")
   self.label_photo = Label(self.home, image=self.photo)
   self.label_photo.grid(row=2,column=5)

   """Frame pour la liste de l'amorce"""
   self.Frame3 = Frame(self.home, borderwidth=4,relief=GROOVE)
   self.Frame3.grid(row=3,column=1)
   self.liste_amorce = Listbox(self.Frame3, height=3)
   self.liste_amorce.insert(1, "Aucune")
   self.liste_amorce.insert(2, "Lettre")
   self.liste_amorce.insert(3, "Chiffre")
   self.liste_amorce.grid(row=1, column=1)

   """Frame pour le Prefixe"""
   self.Frame4 = Frame(self.home,borderwidth=4,relief=GROOVE)
   self.Frame4.grid(row=3,column=2)
   self.Label_prefixe = Label(self.Frame4, text="Prefixe").grid(row=1, column=1)
   self.Input_Prefixe = Entry(self.Frame4, text="Prefixe ?", width=7).grid(row=2, column=1)

   """Frame pour les RadioButton"""
   self.Frame5 = Frame(self.home, borderwidth=4, relief=GROOVE)
   self.Frame5.grid(row=3,column=3)
   self.val = IntVar()
   self.RadioButtonTrue = Radiobutton(self.Frame5, text="Nom Original", variable=self.val, value=1).grid(row=1, column=1)
   self.RadioButtonFalse = Radiobutton(self.Frame5, text="Autre Saisir", variable=self.val, value=2).grid(row=2, column=1)
   self.Entry_Name = Entry(self.Frame5, width=15).grid(row=2,column=2)

   """Frame pour Postfixe"""
   self.Frame6 = Frame(self.home, borderwidth=4, relief=GROOVE)
   self.Frame6.grid(row=3,column=4)
   self.Label_postfixe = Label(self.Frame6, text="PostFixe").grid(row=1, column=1)
   self.Input_postfixe = Entry(self.Frame6, text="postfixe ?", width=10).grid(row=2, column=1)

   """Frame pour les Extensions"""
   self.Frame7 = Frame(self.home, borderwidth=4, relief=GROOVE)
   self.Frame7.grid(row=3,column=5)
   self.Label_Extension = Label(self.Frame7, text="Extension Concernée").grid(row=1, column=1)
   self.Input_Extension = Entry(self.Frame7, text="Extension ?", width=30).grid(row=2, column=1)

   """Frame pour le Button Rename"""
   self.Frame8 = Frame(self.home, borderwidth=4, relief=GROOVE)
   self.Frame8.grid(row=4,column=5)
   self.buton_remane = Button(self.Frame8,text="Renommer", command=Renommage.renommer).grid(row=1, column=1)

   """Frame pour "A Partir de " """
   self.Frame9 = Frame(self.home, borderwidth=4, relief=GROOVE)
   self.Frame9.grid(row=4,column=1)
   self.Label_Partir = Label(self.Frame9, text="A Partir de").grid(row=1, column=1)
   self.Input_partir = Entry(self.Frame9, text="", width=7).grid(row=2, column=1)

   """Menu"""
   self.main_menu = Menu(self.home)
   self.menu1 = Menu(self.main_menu, tearoff=0)
   self.main_menu.add_cascade(label="Règles", menu=self.menu1)
   self.menu1.add_command(label="Lister", command=self.lancercreate)
   self.menu1.add_command(label="Créer", command=self.lancer)
   self.home.config(menu=self.main_menu)

   """Compilation"""
   self.home.mainloop()

  def lancer(self):
    self.home.destroy()
    Annexes2()

  def lancercreate(self):
      self.home.destroy()
      IntLister()

Main = Interface()
