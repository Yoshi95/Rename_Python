from tkinter import *
from Action import *
from ListeRegle import *
from Regle import *
from Renomage import *
from Lister import *
from TKinter import *

class Annexes2():
    """Interface Création Règle"""

    def __init__(self):
        self.rules = Tk()
        self.rules.title("Création Règles")
        self.rules.geometry("1000x500")
        self.rules.resizable(height=FALSE, width=FALSE)
    #Label Vide
        self.Label_Vide = Label(self.rules, text="")
        self.Label_Vide.grid(row=3, column=3)
        self.Label_Vide.grid(row=3, column=2)
    #
        self.Frame1 = Frame(self.rules, borderwidth=2, relief=GROOVE)
        self.Frame1.grid(row=2, column=3)
        self.label_rules = Label(self.Frame1, text="Crée une règle")
        self.label_rules.grid(row=1, column=1)
        self.label_name = Label(self.Frame1, text="Nom de la règles")
        self.label_name.grid(row=2, column=1)
        self.Input_Chemin = Entry(self.Frame1, text="Chemin ?", width=30)
        self.Input_Chemin.grid(row=3, column=1)

        """Frame pour la photo"""
        self.photo = PhotoImage(file="logo.gif")
        self.label_photo = Label(self.rules, image=self.photo)
        self.label_photo.grid(row=2, column=5)

        """Frame pour la liste de l'amorce"""
        self.Frame3 = Frame(self.rules, borderwidth=4, relief=GROOVE)
        self.Frame3.grid(row=3, column=1)
        self.liste_amorce = Listbox(self.Frame3, height=3)
        self.liste_amorce.insert(1, "Aucune")
        self.liste_amorce.insert(2, "Lettre")
        self.liste_amorce.insert(3, "Chiffre")
        self.liste_amorce.grid(row=1, column=1)

        """Frame pour le Prefixe"""
        self.Frame4 = Frame(self.rules, borderwidth=4, relief=GROOVE)
        self.Frame4.grid(row=3, column=2)
        self.Label_prefixe = Label(self.Frame4, text="Prefixe").grid(row=1, column=1)
        self.Input_Prefixe = Entry(self.Frame4, text="Prefixe ?", width=7).grid(row=2, column=1)

        """Frame pour les RadioButton"""
        self.Frame5 = Frame(self.rules, borderwidth=4, relief=GROOVE)
        self.Frame5.grid(row=3, column=3)
        self.val = IntVar()
        self.RadioButtonTrue = Radiobutton(self.Frame5, text="Nom Original", variable=self.val, value=1).grid(row=1,
                                                                                                              column=1)
        self.RadioButtonFalse = Radiobutton(self.Frame5, text="Autre Saisir", variable=self.val, value=2).grid(row=2,
                                                                                                               column=1)
        self.Entry_Name = Entry(self.Frame5, width=15).grid(row=2, column=2)

        """Frame pour Postfixe"""
        self.Frame6 = Frame(self.rules, borderwidth=4, relief=GROOVE)
        self.Frame6.grid(row=3, column=4)
        self.Label_postfixe = Label(self.Frame6, text="PostFixe").grid(row=1, column=1)
        self.Input_postfixe = Entry(self.Frame6, text="postfixe ?", width=10).grid(row=2, column=1)

        """Frame pour les Extensions"""
        self.Frame7 = Frame(self.rules, borderwidth=4, relief=GROOVE)
        self.Frame7.grid(row=3, column=5)
        self.Label_Extension = Label(self.Frame7, text="Extension Concernée").grid(row=1, column=1)
        self.Input_Extension = Entry(self.Frame7, text="Extension ?", width=30).grid(row=2, column=1)

        """Frame pour le Button Rename"""
        self.Frame8 = Frame(self.rules, borderwidth=4, relief=GROOVE)
        self.Frame8.grid(row=4, column=5)
        #self.buton_create = Button(self.Frame8, text="Créer", command=Regle.create).grid(row=1, column=1)

        """Menu"""
        self.main_menu = Menu(self.rules)
        self.menu1 = Menu(self.main_menu, tearoff=0)
        self.main_menu.add_cascade(label="Règles", menu=self.menu1)
        self.menu1.add_command(label="Lister", command=self.lancer)
        self.menu1.add_command(label="Créer", command=self.lancercreate)
        self.rules.config(menu=self.main_menu)

        self.rules.mainloop()


    def lancer(self):
        self.home.destroy()
        Interface()

    def lancercreate(self):
        self.home.destroy()
        IntLister()